(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "+DhY":
/*!*****************************************************!*\
  !*** ./src/app/components/about/about.component.ts ***!
  \*****************************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "3Pt+");




class AboutComponent {
    constructor(fb) {
        this.fb = fb;
        this.crearFormulario();
    }
    ngOnInit() {
    }
    // tslint:disable-next-line: typedef
    crearFormulario() {
        this.forma = this.fb.group({
            nombre: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].minLength(5)]],
            apellido: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            correo: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
        });
    }
    // tslint:disable-next-line: typedef
    guardar() {
        console.log(this.forma);
    }
}
AboutComponent.ɵfac = function AboutComponent_Factory(t) { return new (t || AboutComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"])); };
AboutComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AboutComponent, selectors: [["app-about"]], decls: 28, vars: 1, consts: [["autocomplete", "off", 3, "formGroup", "ngSubmit"], [1, "form-group", "row"], [1, "col-2", "col-form-label"], [1, "col-8"], ["type", "text", "placeholder", "Nombre", "formControlName", "nombre", 1, "form-control"], ["type", "text", "placeholder", "Apellido", "formControlName", "apellido", 1, "form-control"], ["type", "email", "placeholder", "Correo electr\u00F3nico", "formControlName", "correo", 1, "form-control"], [1, "input-group", "col-md-8"], ["type", "submit", 1, "btn", "btn-outline-primary", "btn-block"]], template: function AboutComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Registrate ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " para obtener mayor informacion ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "form", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function AboutComponent_Template_form_ngSubmit_5_listener() { return ctx.guardar(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "label", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Nombre");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "input", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "label", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Apellido");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "input", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "label", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Correo");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "input", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "label", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "\u00A0");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, " Guardar ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.forma);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"]], encapsulation: 2 });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AboutComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-about',
                templateUrl: './about.component.html',
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }]; }, null); })();


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\RESIDENCIA PROFESIONAL\Angular\Pueblos-Magicos\src\main.ts */"zUnb");


/***/ }),

/***/ "1d1H":
/*!***********************************************************!*\
  !*** ./src/app/components/buscador/buscador.component.ts ***!
  \***********************************************************/
/*! exports provided: BuscadorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BuscadorComponent", function() { return BuscadorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _services_pueblos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/pueblos.service */ "XIbN");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");





function BuscadorComponent_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h4", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function BuscadorComponent_div_6_Template_button_click_7_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const i_r2 = ctx.index; const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.verMagico(i_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " mas... ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const magico_r1 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", magico_r1.img, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"])("alt", magico_r1.nombre);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](magico_r1.nombre);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", magico_r1.bio, " ");
} }
class BuscadorComponent {
    // tslint:disable-next-line: no-shadowed-variable
    constructor(activatedRoute, 
    // tslint:disable-next-line: variable-name
    pueblosService) {
        this.activatedRoute = activatedRoute;
        this.pueblosService = pueblosService;
        this.pueblos = [];
    }
    // tslint:disable-next-line: typedef
    ngOnInit() {
        this.activatedRoute.params.subscribe(params => {
            this.termino = params[' termino '];
            this.pueblos = this.pueblosService.buscarPueblos(params[' termino ']);
            console.log(this.pueblos);
        });
    }
    // tslint:disable-next-line: typedef
    verMagico(idx) {
        this.router.navigate(['/magico', idx]);
    }
}
BuscadorComponent.ɵfac = function BuscadorComponent_Factory(t) { return new (t || BuscadorComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_pueblos_service__WEBPACK_IMPORTED_MODULE_2__["PueblosService"])); };
BuscadorComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: BuscadorComponent, selectors: [["app-buscador"]], decls: 7, vars: 2, consts: [["class", "card animated ", 4, "ngFor", "ngForOf"], [1, "card", "animated"], [1, "card-img-top", "img", "fluid", 3, "src", "alt"], [1, "card-block"], [1, "card-title"], [1, "card-text"], ["type", "button", 1, "btn", "btn-outline-primary", "btn-block", 3, "click"]], template: function BuscadorComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " buscando:");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, BuscadorComponent_div_6_Template, 9, 4, "div", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.termino);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.pueblos);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"]], encapsulation: 2 });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](BuscadorComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-buscador',
                templateUrl: './buscador.component.html'
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] }, { type: _services_pueblos_service__WEBPACK_IMPORTED_MODULE_2__["PueblosService"] }]; }, null); })();


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "BuFo":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _services_pueblos_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/pueblos.service */ "XIbN");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");





function HomeComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h4", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_div_7_Template_button_click_7_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const i_r2 = ctx.index; const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.verMagico(i_r2); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " mas... ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const magico_r1 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", magico_r1.img, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"])("alt", magico_r1.nombre);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](magico_r1.nombre);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", magico_r1.bio, " ");
} }
class HomeComponent {
    // tslint:disable-next-line: variable-name
    constructor(_pueblosService, router) {
        this._pueblosService = _pueblosService;
        this.router = router;
        this.pueblos = [];
        // console.log ("constructor");
    }
    // tslint:disable-next-line: typedef
    ngOnInit() {
        this.pueblos = this._pueblosService.getPueblos();
        // console.log(this.pueblos);
    }
    // tslint:disable-next-line: typedef
    verMagico(idx) {
        this.router.navigate(['/magico', idx]);
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_pueblos_service__WEBPACK_IMPORTED_MODULE_1__["PueblosService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 8, vars: 1, consts: [[1, "card-columns"], ["class", "card animated fadIn fast", 4, "ngFor", "ngForOf"], [1, "card", "animated", "fadIn", "fast"], [1, "card-img-top", "img", "fluid", 3, "src", "alt"], [1, "card-block"], [1, "card-title"], [1, "card-text"], ["type", "button", 1, "btn", "btn-outline-primary", "btn-block", 3, "click"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Pueblos M\u00E1gicos");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Lugares hermosos de M\u00E9xico para visitar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, HomeComponent_div_7_Template, 9, 4, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.pueblos);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"]], encapsulation: 2 });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-home',
                templateUrl: './home.component.html',
            }]
    }], function () { return [{ type: _services_pueblos_service__WEBPACK_IMPORTED_MODULE_1__["PueblosService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }]; }, null); })();


/***/ }),

/***/ "GSb+":
/*!*********************************************************!*\
  !*** ./src/app/components/lugares/lugares.component.ts ***!
  \*********************************************************/
/*! exports provided: LugaresComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LugaresComponent", function() { return LugaresComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @agm/core */ "pxUr");



class LugaresComponent {
    constructor() {
        this.lat = 19.4978;
        this.lng = -99.1269;
    }
    ngOnInit() {
    }
}
LugaresComponent.ɵfac = function LugaresComponent_Factory(t) { return new (t || LugaresComponent)(); };
LugaresComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: LugaresComponent, selectors: [["app-lugares"]], decls: 4, vars: 4, consts: [[3, "latitude", "longitude"]], template: function LugaresComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "lugares listo..!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "agm-map", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "agm-marker", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("latitude", ctx.lat)("longitude", ctx.lng);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("latitude", ctx.lat)("longitude", ctx.lng);
    } }, directives: [_agm_core__WEBPACK_IMPORTED_MODULE_1__["AgmMap"], _agm_core__WEBPACK_IMPORTED_MODULE_1__["AgmMarker"]], encapsulation: 2 });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LugaresComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-lugares',
                templateUrl: './lugares.component.html',
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "RUEf":
/*!*******************************!*\
  !*** ./src/app/app.routes.ts ***!
  \*******************************/
/*! exports provided: APP_ROUTING */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APP_ROUTING", function() { return APP_ROUTING; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/home/home.component */ "BuFo");
/* harmony import */ var _components_about_about_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/about/about.component */ "+DhY");
/* harmony import */ var _components_magico_magico_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/magico/magico.component */ "XPTs");
/* harmony import */ var _components_lugares_lugares_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/lugares/lugares.component */ "GSb+");
/* harmony import */ var _components_buscador_buscador_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/buscador/buscador.component */ "1d1H");






const APP_ROUTES = [
    { path: 'home', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_1__["HomeComponent"] },
    { path: 'about', component: _components_about_about_component__WEBPACK_IMPORTED_MODULE_2__["AboutComponent"] },
    { path: 'lugares', component: _components_lugares_lugares_component__WEBPACK_IMPORTED_MODULE_4__["LugaresComponent"] },
    { path: 'magico/:id', component: _components_magico_magico_component__WEBPACK_IMPORTED_MODULE_3__["MagicoComponent"] },
    { path: 'buscar/:termino', component: _components_buscador_buscador_component__WEBPACK_IMPORTED_MODULE_5__["BuscadorComponent"] },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];
const APP_ROUTING = _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(APP_ROUTES);


/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/navbar/navbar.component */ "hrlM");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");




class AppComponent {
    constructor() {
        this.title = 'Pueblos-Magicos';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 3, vars: 0, consts: [[1, "container", "main-container"]], template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "app-navbar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_1__["NavbarComponent"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterOutlet"]], styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.css']
            }]
    }], null, null); })();


/***/ }),

/***/ "XIbN":
/*!*********************************************!*\
  !*** ./src/app/services/pueblos.service.ts ***!
  \*********************************************/
/*! exports provided: PueblosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PueblosService", function() { return PueblosService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "tk/3");



class PueblosService {
    constructor(http) {
        this.http = http;
        this.pueblos = [
            {
                nombre: 'Cholula',
                bio: 'Pueblo Mágico ubicado en el estado de Puebla.',
                img: 'assets/img/cholula.png',
                pais: 'País México',
                descripcion: 'Si en Puebla se muestra el arte novohispano en todo su esplendor, en Cholula se respiran profundos aires prehispánicos. La ciudad fue levantada sobre los cimientos de una importante urbe indígena en donde se edificaron diversos templos y adoratorios, sobre los cuales más tarde se construyeron templos católicos; hoy Cholula tiene 37 iglesias, algo sorprendente para una ciudad de su tamaño.'
            },
            {
                nombre: 'Tlayacapan',
                bio: 'Pueblo Mágico ubicado en el estado de Morelos.',
                img: 'assets/img/tlayacapan.png',
                pais: 'País México',
                descripcion: 'Su encanto reside en sus costumbres, sus tradiciones heredadas de la cultura Olmeca y la ocupación de los Xochimilcas, quienes se establecieron y dominaron esta localidad en la época prehispánica. Su nombre viene del náhuatl y significa “sobre la punta de la tierra”, “lugar de los límites o linderos” o “la nariz de la tierra” y es de los pocos pueblos que conserva gran parte de su traza urbana prehispánica original.'
            },
            {
                nombre: 'Tepoztlan',
                bio: 'Pueblo Mágico ubicado en el estado de Morelos.  ',
                img: 'assets/img/tepoztlan.png',
                pais: 'País México',
                descripcion: 'Tepotzotlán es un pueblo que conserva el toque de provincia a pesar de ser parte del área Metropolitana que rodea la Ciudad de México.Uno de los espacios que lo distinguen es la Plaza de la Cruz, cuyo valor radica en la cruz atrial de piedra labrada con imágenes de la Pasión de Cristo.'
            },
            {
                nombre: 'Aculco de Espinoza',
                bio: 'Pueblo Mágico ubicado en el estado de México. ',
                img: 'assets/img/aculco.png',
                pais: 'País México',
                descripcion: 'Este Pueblo Mágico reside entre montañas, peñas y cascadas, donde el sol inunda de luz calles, callejones y refleja su luz en sus blancas paredes. Así comienza cada día el verdadero Aculco, ven a encontrarte con Otomíes, y descubrir porque escogieron este lugar.'
            },
            {
                nombre: 'Álamos',
                bio: 'Pueblo Mágico ubicado en el estado de Sonora.  ',
                img: 'assets/img/alamos.png',
                pais: 'País México',
                descripcion: 'Muy cerca de la frontera con Sinaloa y Chihuahua se encuentra Álamos, un Pueblo Mágico que sorprende por su arquitectura y tradiciones, resultado de la fusión del barroco español y de nuestros pueblos originarios. Al pasear por sus calles empedradas y callejones se pueden admirar antiguas casonas muy bien conservadas que cuentan historias de otras épocas, especialmente del auge minero debido a la veta de La Europea.'
            },
            {
                nombre: 'Amealco de Buenfil',
                bio: 'Pueblo Mágico ubicado en el estado de Querétaro. ',
                img: 'assets/img/amealco.png',
                pais: 'País México',
                descripcion: 'Este Pueblo Mágico siempre ha tenido mucho que ofrecer a sus visitantes. Es un lugar especial por sus cuerpos de agua, y sus paisajes boscosos. En Amealco se pueden realizar distintas actividades tales como practicar campismo, ciclismo de montaña, senderismo, ecoturismo y rutas en cuatrimoto.'
            },
            {
                nombre: 'Aquismon',
                bio: 'Pueblo Mágico ubicado en el estado de San Luis Potosí. ',
                img: 'assets/img/aquismon.png',
                estado: 'San Luis Potosí',
                pais: 'País México',
                descripcion: 'El Pueblo Mágico de Aquismón es la Meca para los amantes de los deportes de aventura como: abseiling, climbing, hiking, kayaking, mountain biking, parachiting rafting, o wingsuit flying; ya que aquí te podrás lanzar a la conquista de espectaculares ríos, cascadas y los impresionantes sótanos y cavernas que abundan en la zona.'
            },
            {
                nombre: 'Arteaga',
                bio: 'Pueblo Mágico ubicado en el estado de Coahuila. ',
                img: 'assets/img/arteaga.png',
                pais: 'País México',
                descripcion: 'Un Pueblo Mágico en medio de una inmensa zona de bosques con pinos y valles de manzanos de los más impresionantes del país. Poseedor de un clima sumamente agradable donde predomina el viento fresco a cualquier hora del día. Arteaga es un destino que por sus bellos paisajes boscosos y cumbres nevadas en invierno, se considera “La Suiza de México”.'
            },
            {
                nombre: 'Bacalar',
                bio: 'Pueblo Mágico ubicado en el estado de Quintana Roo. ',
                img: 'assets/img/bacalar.png',
                pais: 'País México',
                descripcion: 'Bacalar o Laguna de los Siete Colores, es una laguna ubicada en el estado de Quintana Roo, muy cerca de Chetumal, la capital del estado. Su nombre viene del maya Bakhalal que significa “cerca o rodeado de carrizos”. De forma circular, la laguna es poco profunda lo que permite nadar cómodamente. Alrededor se encuentran establecidas tanto casas privadas como pequeñas posadas que permiten acceso a la laguna.'
            },
            {
                nombre: 'Batopilas',
                bio: 'Pueblo Mágico ubicado en el estado de Chihuahua. ',
                img: 'assets/img/batopilas.png',
                pais: 'País México',
                descripcion: 'Batopilas se localiza en las entrañas de una de las barrancas más profundas de la Sierra Madre Occidental, llamada también Sierra Tarahumara; para llegar al Pueblo Mágico, se toma el ferrocarril Chepe en Chihuahua y se hace escala en Creel, a partir de ahí, se viaja por 3 horas en una de las carreteras más espectaculares no sólo de México, sino del mundo.'
            },
            {
                nombre: 'Atlixco',
                bio: 'Pueblo Mágico ubicado en el estado de Puebla. ',
                img: 'assets/img/Atlixco.jpg',
                pais: 'País México',
                descripcion: 'Este Pueblo Mágico convive con el volcán Popocatépetl. Donde te ubiques, sea una terraza o en la punta del Cerro de San Miguel, el coloso siempre vigila y recuerda su presencia a través de una bocanada. Visitar Atlixco es un aventura aromática y multicolor. Este pueblo presume su clima como superior al del resto de México pues aquí hay árboles, flores, frutas y plantas por dondequiera.'
            },
            {
                nombre: 'Ajijic',
                bio: 'Pueblo Mágico ubicado en el estado de Jalisco. ',
                img: 'assets/img/Ajijic.jpg',
                pais: 'País México',
                descripcion: 'Fundada en 1531, su nombre proviene del náhuatl Axixic que significa "donde se derrama el agua" o “donde brota el agua”, es una de las villas más antiguas de México. Mucho antes de la conquista española ocupó esta zona, en las orillas del Lago de Chapala, una pequeña comunidad náhuatl y nombraron a este lugar -según su mitología- uno de los cuatro puntos cardinales, en referencia al clima y energía poderosa de la región.'
            },
            {
                nombre: 'Bernal',
                bio: 'Pueblo Mágico ubicado en el estado de Querétaro. ',
                img: 'assets/img/Bernal.jpg',
                pais: 'País México',
                descripcion: 'Es un Pueblo Mágico lleno de colores y vida donde puedes pasar un fin de semana en familia gozando de la tranquilidad y buen clima que ofrece mientras disfrutan de la gastronomía y las artesanías locales. En este pueblo se encuentra uno de los atractivos turísticos más importantes de Querétaro, la Peña de Bernal.'
            },
            {
                nombre: 'Bustamante',
                bio: 'Pueblo Mágico ubicado en el estado de Nuevo León. ',
                img: 'assets/img/Bustamante.jpg',
                pais: 'País México',
                descripcion: 'Este Pueblo Mágico es conocido como “El jardín de Nuevo León” ya que es el oasis de la región. El verdor de los árboles rodea a Bustamante y permite adentrarse en la calma de sus calles que nos recibe para encontrar una arquitectura típica. Los conquistadores españoles trajeron a unas 30 familias Tlaxcaltecas para trabajar en las minas, ellos dieron forma y color al pueblo; a la vez que heredaron al “Señor de Tlaxcala”. Por ello es la última chispa de herencia tlaxcalteca.'
            },
            {
                nombre: 'Cadereyta de Montes',
                bio: 'Pueblo Mágico ubicado en el estado de Querétaro. ',
                img: 'assets/img/Cadereyta.jpg',
                pais: 'País México',
                descripcion: 'Visitar este Pueblo Mágico es una experiencia única ya que es la entrada a la Sierra Gorda queretana; es un municipio con gran diversidad geográfica: tiene clima semidesértico en el sur y bosques en el norte. En 2011 fue nombrado Pueblo Mágico gracias a su historia y a su magia heredada de los pueblos originarios de la región, así como por sus diversos atractivos turísticos.'
            },
            {
                nombre: 'Calvillo',
                bio: 'Pueblo Mágico ubicado en el estado de Aguascalientes.',
                img: 'assets/img/calvillo.png',
                pais: 'País México',
                descripcion: 'Recibe su nombramiento como Pueblo Mágico en noviembre de 2012. Ubicado en el estado de Aguascalientes, lleva el nombre de Calvillo, en honor a su benefactor. Su origen se da con el establecimiento de una congregación de indios nahuas de San José de Huejúcar, que significa “lugar de sauces”.'
            },
            {
                nombre: 'Candela',
                bio: 'Pueblo Mágico ubicado en el estado de Coahuila.',
                img: 'assets/img/candela.jpg',
                pais: 'País México',
                descripcion: 'Un Pueblo Mágico que también es heroico, lleno de historia y cultura pues fue punto clave de batallas revolucionarias Lideradas por Venustiano Carranza. Recibe a visitantes que buscan sus aguas termales de Ojo Caliente y los paisajes de Los Carricitos, Las Lajitas y el Frentón.'
            },
            {
                nombre: 'Capulálpam de Mendez',
                bio: 'Pueblo Mágico ubicado en el estado de Oaxaca.',
                img: 'assets/img/capulalpam.png',
                pais: 'País México',
                descripcion: 'Enclavada en la Sierra Norte de Oaxaca se encuentra la localidad de Capulálpam de Méndez. Sus casas perfectamente alineadas con paredes de adobe y techos de teja, entre calles empedradas y enmarcadas en la cantera amarilla del templo de San Mateo, fueron algunas de las razones por las cuales, junto con sus tradiciones y atractivos naturales, fue denominado, como Pueblo Mágico.'
            },
            {
                nombre: 'Casas Grandes',
                bio: 'Pueblo Mágico ubicado en el estado de Chihuahua.',
                img: 'assets/img/Casas.jpg',
                pais: 'País México',
                descripcion: 'Las primeras noticias de ésta enigmática ciudad se tuvieron en el informe del conquistador Don Francisco de Ibarra en 1566, quien la describió como “de mucha grandeza, altura y fortaleza”, ya que había edificios de hasta 7 pisos. En la antigua ciudad de Paquimé, origen de Casas Grandes, se descubrieron asentamientos que datan del año 600 de nuestra era. Al arribo de los españoles, la ciudad ya había sido abandonada por sus habitantes originales. Se cree que sucedió alrededor del siglo VI.'
            },
            {
                nombre: 'Chiapa de Corzo',
                bio: 'Pueblo Mágico ubicado en el estado de Chiapas.',
                img: 'assets/img/chiapa.jpg',
                pais: 'País México',
                descripcion: 'Es un hermoso Pueblo Mágico considerado una de las poblaciones más antiguas del continente americano, pues se fundó en marzo de 1528. Al inicio fue habitado por los españoles que llegaron al estado pero, debido al caluroso clima de la región estos decidieron migrar a lo que hoy es San Cristóbal de las Casas, de esta forma, la antigua Chiapa quedó habitada en sus inicios por los frailes Dominicos y los indígenas de la región, por lo que era llamada “Chiapa de los Indios”. Su nombre actual le fue puesto en honor de Don Ángel Albino Corzo, destacado político liberal chiapacorceño.'
            },
            {
                nombre: 'Chignahuapan',
                bio: 'Pueblo Mágico ubicado en el estado de Puebla.',
                img: 'assets/img/chignaguapan.png',
                pais: 'País México',
                descripcion: 'Chignahuapan, Pueblo Mágico, también tiene fama de ser el mayor productor de esferas navideñas en el país, albergando fábricas y talleres artesanales que han mantenido vivo el oficio durante varias generaciones. Vive sus verbenas invernales los fines de semana en el Tianguis de Esferas Navideñas y disfruta de tus compras.'
            },
            {
                nombre: 'Coatepec',
                bio: 'Pueblo Mágico ubicado en el estado de Veracruz.',
                img: 'assets/img/coatepec.jpg',
                pais: 'País México',
                descripcion: 'Declarado Patrimonio Histórico de la Nación, el pueblo alberga más de 350 inmuebles con gran valor histórico, que valieron para posicionar al destino dentro de la lista de Pueblos Mágicos de México. Esto, combinado con su producción cafetalera, sus tradiciones populares y religiosas, han hecho de Coatepec un lugar en la mira de miles de viajeros.'
            },
            {
                nombre: 'Comala',
                bio: 'Pueblo Mágico ubicado en el estado de Colima.',
                img: 'assets/img/comala.jpg',
                pais: 'País México',
                descripcion: 'Entre sus calles empedradas y sus sencillas casas blancas adornadas con faroles, puede verse desde temprano la rutina de sus habitantes que, pausadamente y sin prisa, acuden a sus jornadas de trabajo y se pierden a lo lejos entre los árboles de papaya.'
            },
            {
                nombre: 'Comitán',
                bio: 'Pueblo Mágico ubicado en el estado de Chiapas.',
                img: 'assets/img/comitan.jpg',
                pais: 'País México',
                descripcion: 'De clima caluroso y húmedo la mayor parte del año, Comitán de Domínguez es conocido por ser la cuna de la Independencia de Chiapas, pues antes de pertenecer a México, fue parte de Guatemala, permaneciendo unos años como país independiente para después unirse nuevamente a los estados que conforman la República Mexicana.'
            },
            {
                nombre: 'Comonfort',
                bio: 'Pueblo Mágico ubicado en el estado de Guanajuato.',
                img: 'assets/img/comonfort.jpg',
                pais: 'País México',
                descripcion: 'Al caminar por las coloridas calles de este Pueblo Mágico, gozarás de un ambiente tranquilo, ideal para admirar su bella arquitectura. En el centro, se puede ver el edificio antiguo del Palacio Municipal hermosamente decorado con murales que narran la historia de Comonfort desde la época prehispánica hasta nuestros días.'
            },
            {
                nombre: 'Compostela de Indias',
                bio: 'Pueblo Mágico ubicado en el estado de Nayarit.',
                img: 'assets/img/compostela.jpg',
                pais: 'País México',
                descripcion: 'Compostela, Pueblo Mágico cuyo su significado es “campo de estrellas” es un lugar con un importante pasado histórico. Lleva un nombre glorioso e inmortal: Santiago de Compostela, por mandato de la reina Juana de Castilla. Este es un lugar lleno de historia y lindas calles con un toque colonial debido a que fue fundado por la colonia española y el Vaticano. Posee hermosos paisajes y su clima es lo que mejor lo describe.'
            },
            {
                nombre: 'Cosalá',
                bio: 'Pueblo Mágico ubicado en el estado de Sinaloa.',
                img: 'assets/img/cosala.jpg',
                pais: 'País México',
                descripcion: 'Sinaloa es conocido por paisajes como el de Mazatlán, pero hay que recordar que su territorio también comprende una parte de la Sierra Madre Occidental, ahí es donde se encuentran pequeños pueblos con mucho encanto como Cosalá, en el que pareciera que el tiempo se detuvo en su arquitectura y la traza de sus calles.'
            },
            {
                nombre: 'Coscomatepec',
                bio: 'Pueblo Mágico ubicado en el estado de Veracruz.',
                img: 'assets/img/coscomatepec.png',
                pais: 'País México',
                descripcion: 'El nombre de Coscomatepec proviene del vocablo náhuatl cuezcomatepec, que se traduce como “Cerro de las Trojes”, es decir, el lugar donde se almacenaban las semillas o los frutos. Pero un capítulo heroico posterior terminó de componer su actual nombre.'
            },
            {
                nombre: 'Creel',
                bio: 'Pueblo Mágico ubicado en el estado de Chihuahua.',
                img: 'assets/img/creel.jpg',
                pais: 'País México',
                descripcion: 'Chihuahua es el estado más grande de México y esto no solo tiene que ver con su extensión geográfica sino también con la variedad de sus paisajes, los cuales parecieran tesoros muy bien resguardados en las imponentes montañas que integran la Sierra Madre Occidental, uno de estos es el Pueblo Mágico de Creel.'
            },
            {
                nombre: 'Cuatro cienegas',
                bio: 'Pueblo Mágico ubicado en el estado de Coahuila.',
                img: 'assets/img/cuatro.jpg',
                pais: 'País México',
                descripcion: 'Cuatro Ciénegas es uno de los Pueblos Mágicos más recientes. Muchos viajeros, al conocer este hermoso lugar se preguntaban desde hace años por qué no tenía el reconocimiento, cuando de sobra se respira en sus callecitas polvorientas y blanqueadas por el sol toda la magia que se puede pedir.'
            },
            {
                nombre: 'Cuetzalan del Progreso',
                bio: 'Pueblo Mágico ubicado en el estado de Puebla.',
                img: 'assets/img/cuetzalan.png',
                pais: 'País México',
                descripcion: 'Camina por sus calles empedradas que parecen escenarios de cuento y la tranquilidad de su ambiente. Los fines de semana se coloca un mercado tradicional en la plaza principal donde se puede poner en práctica tradiciones prehispánicas como el trueque o cambio de productos locales evitando el uso de dinero. Una buena opción para desarrollar tus habilidades para negociar.'
            },
            {
                nombre: 'Cuitzeo del Porvenir',
                bio: 'Pueblo Mágico ubicado en el estado de Michoacán.',
                img: 'assets/img/cuatro.jpg',
                pais: 'País México',
                descripcion: 'La magia inicia desde la carretera, en el momento que vas de Morelia al pueblo. Lo que comienza como un viaje en automóvil de pronto se convierte en una navegación, pues casi sin darte cuenta, parecerá que has abordado una embarcación.'
            },
            {
                nombre: 'El Oro',
                bio: 'Pueblo Mágico ubicado en el estado de México.',
                img: 'assets/img/oro.png',
                pais: 'País México',
                descripcion: 'El Pueblo Mágico de El Oro, en el Estado de México, es el lugar perfecto si buscas un destino para salir el fin de semana y deshacerte del ajetreo cotidiano. Se encuentra a dos horas y media de la Ciudad de México, a hora y media de Toluca; colinda al norte de Temascalcingo y al oeste con el estado de Michoacán. '
            },
            {
                nombre: 'El Rosario',
                bio: 'Pueblo Mágico ubicado en el estado de Sinaloa.',
                img: 'assets/img/rosario.jpg',
                pais: 'País México',
                descripcion: 'La historia del Pueblo Mágico de El Rosario, empieza con una leyenda ¿o una historia real? Corría el año 1655 y al caporal Bonifacio Rojas se le escapó una vaca que encontró en la loma de Santiago, luego de buscarla un buen rato. Pero para entonces ya caía la noche y el ganadero debió elegir un lugar donde encender una fogata para iluminarse mientras esperaba el amanecer. Pero al despertar al día siguiente, Bonifacio Rojas se sorprendió al ver que bajo las cenizas había plata fundida.'
            },
            {
                nombre: 'El Fuerte',
                bio: 'Pueblo Mágico ubicado en el estado de Sinaloa.',
                img: 'assets/img/fuerte.jpg',
                pais: 'País México',
                descripcion: 'Recorrer los caminos de El Fuerte, Pueblo Mágico de Sinaloa, es entender el concepto de resistencia, pues pese a la conquista conserva los últimos eslabones de su pasado intactos, protegiéndose de la invasión.'
            },
            {
                nombre: 'Guadalupe',
                bio: 'Pueblo Mágico ubicado en el estado de Zacatecas.',
                img: 'assets/img/guadalupe.jpg',
                pais: 'País México',
                descripcion: 'Este Pueblo Mágico de calles tranquilas, y de gente trabajadora te invita a conocer porqué es diferente, aún cuando colinda, y es parte de la zona metropolitana de Zacatecas. Este lugar forma parte del “Camino Real de Tierra Adentro”, declarado Patrimonio de la Humanidad por la UNESCO.'
            },
            {
                nombre: 'Guerrero',
                bio: 'Pueblo Mágico ubicado en el estado de Coahuila.',
                img: 'assets/img/guerrero.jpg',
                pais: 'País México',
                descripcion: 'Este Pueblo Mágico es un ejemplo de sobrevivencia al tiempo, aquí encontrarás fascinantes huellas de su pasado novohispano. Camina y recorre sus calles pobladas de gente franca y animosa que te invita a sonreír, entra en su antigua iglesia que revela el olor a madera de antaño y protege un tesoro: imágenes de santos del siglo XVII o XVIII, y junto a las vestigios de La Misión de San Bernardo que te transportarán a épocas de la conquista.'
            },
            {
                nombre: 'Huamantla',
                bio: 'Pueblo Mágico ubicado en el estado de Tlaxcala.',
                img: 'assets/img/huamantla.jpg',
                pais: 'País México',
                descripcion: 'Entre los muchos tesoros que Tlaxcala guarda puedes descubrir Huamantla, un sitio ideal para pasar un fin de semana tranquilo en la naturaleza o, bien, descubriendo tradiciones centenarias que sobreviven en sus haciendas.'
            },
            {
                nombre: 'Huasca de ocampo',
                bio: 'Pueblo Mágico ubicado en el estado de Oaxaca.',
                img: 'assets/img/huasca.jpg',
                pais: 'País México',
                descripcion: 'Primer Pueblo Mágico de México, destacado por sus increíbles paisajes naturales, lugares históricos, hermosas vistas, calles empedradas, paseos románticos, haciendas coloniales, lugares mágicos, donde se combinan lo hecho por el hombre y la obra de la naturaleza, creando una imagen agradable y acogedora para pasar unos días de verdadero descanso en esta pintoresca provincia.'
            },
            {
                nombre: 'Huachinango',
                bio: 'Pueblo Mágico ubicado en el estado de Puebla.',
                img: 'assets/img/huachinango.jpg',
                pais: 'País México',
                descripcion: 'El Pueblo Mágico de Huauchinango está rodeado por cerros, ríos que descienden en cascadas y barrancas profundas pertenecientes a la Sierra Norte de Puebla. Por lo tanto, es un destino ideal para escapar un fin de semana y hacer turismo de aventura y naturaleza.'
            },
            {
                nombre: 'Huautla de Jiménez',
                bio: 'Pueblo Mágico ubicado en el estado de Oaxaca.',
                img: 'assets/img/huatla.jpg',
                pais: 'País México',
                descripcion: 'Huautla, rodeada por la hermosa vegetación que solo se da en la Sierra Mazateca, pareciera un escenario que invita a la meditación entre sus montañas, ríos, cascadas y grutas que conforman su paisaje mezclándose con el olor del cacao, incienso y café que se cultivan en sus tierras.'
            },
            {
                nombre: 'Huichapan ',
                bio: 'Pueblo Mágico ubicado en el estado de Hidalgo.',
                img: 'assets/img/huichapan.jpg',
                pais: 'País México',
                descripcion: 'Por su ubicación, Hidalgo es un lugar perfecto para pensar en escapadas románticas de fin de semana o viajes en familia dedicados a pasar momentos al aire libre o explorando tesoros de la arquitectura colonial. Entre los muchos lugares en los que podrás crear nuevos recuerdos está Huichapan, un Pueblo Mágico que conquistará todos tus sentidos.'
            },
            {
                nombre: 'iztapa de la sal',
                bio: 'Pueblo Mágico ubicado en el estado de México.',
                img: 'assets/img/iztapa.jpg',
                pais: 'País México',
                descripcion: 'Al sur del Estado de México se localiza Ixtapan de la Sal, Pueblo Mágico frecuentado por sus relajantes aguas termales y balnearios, que se disfrutan con su clima templado y su agradable ambiente familiar. En sus alrededores también hallarás Spas, cascadas y parques para realizar deportes de aventura, disfrutar de la naturaleza y romancear si vas en pareja. '
            },
            {
                nombre: 'Isla Aguda',
                bio: 'Pueblo Mágico ubicado en el estado de Campeche.',
                img: 'assets/img/aguda.jpg',
                pais: 'País México',
                descripcion: 'Es realmente un pueblo pequeño en el que los pescadores salen temprano por la mañana a trabajar y en el que las distintas tonalidades azules del mar crean un espectáculo visual que no te puedes perder.'
            },
            {
                nombre: 'Isla Mujeres',
                bio: 'Pueblo Mágico ubicado en el estado de Quintana Roo.',
                img: 'assets/img/mujeres.jpg',
                pais: 'País México',
                descripcion: 'Este pedacito de tierra, de apenas siete kilómetros de longitud, es el primer lugar de México al que tocan los rayos del sol cada mañana. Para ver cómo el astro rey se levanta sobre sus aguas cristalinas y turquesas, debes viajar a Punta Sur. Pero, esta no es la única razón para ir, pues cada año, de mayo a septiembre, esta zona del país se engalana con la llegada del tiburón ballena, el pez más grande del mundo, con el que puedes esnorquelear. Los lugareños aseguran que se pueden ver hasta 300 ejemplares.'
            },
            {
                nombre: 'Izamal',
                bio: 'Pueblo Mágico ubicado en el estado de Yucatan.',
                img: 'assets/img/izamal.jpg',
                pais: 'País México',
                descripcion: 'Testimonio de tres culturas: un encanto colonial con el sello distintivo de un pueblo pequeño, magníficos testimonios de la cultura maya y un gran convento de influencia española. Este destino tiene un legado cultural importante. Todas las casas, las tiendas y las iglesias de este lugar están pintadas de amarillo dorado y al pueblo se le conoce como "La Ciudad Amarilla".'
            },
            {
                nombre: 'Jala',
                bio: 'Pueblo Mágico ubicado en el estado de Nayarit.',
                img: 'assets/img/jala.jpg',
                pais: 'País México',
                descripcion: 'Jala es una población perteneciente al municipio de Jala, en el Estado de Nayarit. Cuenta con 5586 habitantes. Jala se encuentra a 1070 metros sobre el nivel del mar (SNM). Jala es el municipio del estado de Nayarit que cuenta con la más diversa cantidad de monumentos históricos, entre los que destacan: su templo parroquial, edificado en la segunda mitad del siglo XIX y las ruinas del antiguo hospital con su fachada Barroca.'
            },
            {
                nombre: 'Jalpa de Cánovas',
                bio: 'Pueblo Mágico ubicado en el estado de Guanajuato.',
                img: 'assets/img/canovas.jpg',
                pais: 'País México',
                descripcion: 'Este lugar pintoresco está situado en los límites del territorio guanajuatense, en el municipio de Purísima del Rincón, a 47 minutos de León. Este mágico lugar forma parte de una serie de antiguas haciendas y pequeños poblados cuya apacible vida provinciana brinda una experiencia única. Su bondadosa tierra la convirtió en uno de los principales productores de alimentos, haciéndose acreedora al nombre de “El Granero de México”.'
            },
            {
                nombre: 'Jalpa de Serra',
                bio: 'Pueblo Mágico ubicado en el estado de Querétaro.',
                img: 'assets/img/serra.jpg',
                pais: 'País México',
                descripcion: 'La Sierra Gorda queretana tiene un gran corazón natural y cultural que late con fuerza en Jalpan de Serra, Pueblo Mágico ubicado a unos 190 kilómetros de la ciudad de Querétaro, para llegar es necesario tomar una carretera con muchas curvas que se recorre en unas tres horas y media.'
            },
            {
                nombre: 'Jerez de García Salinas',
                bio: 'Pueblo Mágico ubicado en el estado de Zacatecas.',
                img: 'assets/img/jerez.jpeg',
                pais: 'País México',
                descripcion: 'El Pueblo Mágico de Jerez, en Zacatecas, es sinónimo de música de tambora, edificios de cantera del siglo XIX, hilos de plata tejidos en arracadas, nieves de mango y tostadas de cueritos y los versos del poeta mexicano Ramón López Velarde. Todo esto se conjuga en un extenso y casi plano valle que marca la puerta de entrada al Cañón de Tlaltenango.'
            },
            {
                nombre: 'Jiquilpa de Juárez',
                bio: 'Pueblo Mágico ubicado en el estado de Michoacan.',
                img: 'assets/img/jiquilpa.jpg',
                pais: 'País México',
                descripcion: 'Jiquilpan de Juárez es uno de los ocho Pueblos Mágicos de Michoacán y se localiza muy cerca del Lago de Chapala. Esta es la tierra que vio nacer al general Lázaro Cárdenas y donde las jacarandas tiñen de morado sus calles durante la primavera. También es un rincón para admirar el arte de José Clemente Orozco y de las mujeres indígenas expertas en tejer rebozos.'
            },
            {
                nombre: 'Lagos de Moreno',
                bio: 'Pueblo Mágico ubicado en el estado de Jalisco.',
                img: 'assets/img/lagos.jpg',
                pais: 'País México',
                descripcion: 'Localizado al noreste de Jalisco, este hermoso Pueblo Mágico, donde su centro histórico y su famoso puente sobre el Río Lagos fueron nombrados Patrimonio Mundial por la UNESCO en 2010, ofrece al visitante actividades como paseos, espectáculos de charrería, y hermosas construcciones religiosas como la Parroquia de la Asunción, gran ejemplo del barroco mexicano y obra maestra de la arquitectura colonial, construida en 1741.'
            },
            {
                nombre: 'Linares',
                bio: 'Pueblo Mágico ubicado en el estado de Nuevo León.',
                img: 'assets/img/linares.jpg',
                pais: 'País México',
                descripcion: 'Enclavado en la región citrícola del estado de Nuevo León se encuentra Linares, un Pueblo Mágico que va más allá de su historia, donde el aroma del dulce de leche invade la ciudad, mientras que su arquitectura refleja la colonización de la región.'
            },
        ];
        this.url = 'http://pruebas.saers-technologies.com/WebServicePueblerinos/WebService/';
    }
    // tslint:disable-next-line: typedef
    getpueblos(url) {
        return this.http.get(url);
    }
    getPueblos() {
        return this.pueblos;
    }
    // tslint:disable-next-line: typedef
    getMagico(idx) {
        return this.pueblos[idx];
    }
    buscarPueblos(termino) {
        const Arr = [];
        termino = termino.toLowerCase();
        for (const pueblito of this.pueblos) {
            const nombre = pueblito.nombre.toLowerCase();
            if (nombre.indexOf(termino) >= 0) {
                Arr.push(pueblito);
            }
        }
        return Arr;
    }
}
PueblosService.ɵfac = function PueblosService_Factory(t) { return new (t || PueblosService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
PueblosService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: PueblosService, factory: PueblosService.ɵfac });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PueblosService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "XPTs":
/*!*******************************************************!*\
  !*** ./src/app/components/magico/magico.component.ts ***!
  \*******************************************************/
/*! exports provided: MagicoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MagicoComponent", function() { return MagicoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _services_pueblos_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/pueblos.service */ "XIbN");




const _c0 = function () { return ["/magico"]; };
class MagicoComponent {
    constructor(activatedRoute, pueblosService) {
        this.activatedRoute = activatedRoute;
        this.pueblosService = pueblosService;
        this.magico = {};
        this.activatedRoute.params.subscribe(params => {
            // tslint:disable-next-line: no-string-literal
            this.magico = this.pueblosService.getMagico(params['id']);
        });
    }
}
MagicoComponent.ɵfac = function MagicoComponent_Factory(t) { return new (t || MagicoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_pueblos_service__WEBPACK_IMPORTED_MODULE_2__["PueblosService"])); };
MagicoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: MagicoComponent, selectors: [["app-magico"]], decls: 23, vars: 8, consts: [[1, "row"], [1, "col", "text-center"], ["src", "assets/img/img1.png", "alt", "pueblos magico"], [1, "img-fluid", 3, "src", "alt"], [1, "btn", "btn-outline-danger", "btn-block", 3, "routerLink"], [1, "col-md-8"]], template: function MagicoComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "small");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "img", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Regresar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "hr");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.magico.nombre);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.magico.bio);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", ctx.magico.img, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"])("alt", ctx.magico.nombre);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](7, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.magico.nombre);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.magico.descripcion, " ");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"]], encapsulation: 2 });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MagicoComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-magico',
                templateUrl: './magico.component.html',
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] }, { type: _services_pueblos_service__WEBPACK_IMPORTED_MODULE_2__["PueblosService"] }]; }, null); })();


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _app_routes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.routes */ "RUEf");
/* harmony import */ var _services_pueblos_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/pueblos.service */ "XIbN");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/home/home.component */ "BuFo");
/* harmony import */ var _components_about_about_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/about/about.component */ "+DhY");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/navbar/navbar.component */ "hrlM");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _components_magico_magico_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/magico/magico.component */ "XPTs");
/* harmony import */ var _components_lugares_lugares_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/lugares/lugares.component */ "GSb+");
/* harmony import */ var _components_buscador_buscador_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/buscador/buscador.component */ "1d1H");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/platform-browser/animations */ "R1ws");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @agm/core */ "pxUr");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/router */ "tyNb");


// rutas

// servicios


// componentes













class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [
        _services_pueblos_service__WEBPACK_IMPORTED_MODULE_3__["PueblosService"]
    ], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            _agm_core__WEBPACK_IMPORTED_MODULE_14__["AgmCoreModule"].forRoot({
                apiKey: 'AIzaSyDZ1RW72H44t3-lNJyvdqjzIOapDOexa2w'
            }),
            _app_routes__WEBPACK_IMPORTED_MODULE_2__["APP_ROUTING"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__["BrowserAnimationsModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
        _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__["NavbarComponent"],
        _components_home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"],
        _components_about_about_component__WEBPACK_IMPORTED_MODULE_6__["AboutComponent"],
        _components_magico_magico_component__WEBPACK_IMPORTED_MODULE_10__["MagicoComponent"],
        _components_lugares_lugares_component__WEBPACK_IMPORTED_MODULE_11__["LugaresComponent"],
        _components_buscador_buscador_component__WEBPACK_IMPORTED_MODULE_12__["BuscadorComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _agm_core__WEBPACK_IMPORTED_MODULE_14__["AgmCoreModule"], _angular_router__WEBPACK_IMPORTED_MODULE_15__["RouterModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__["BrowserAnimationsModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                    _components_navbar_navbar_component__WEBPACK_IMPORTED_MODULE_8__["NavbarComponent"],
                    _components_home_home_component__WEBPACK_IMPORTED_MODULE_5__["HomeComponent"],
                    _components_about_about_component__WEBPACK_IMPORTED_MODULE_6__["AboutComponent"],
                    _components_magico_magico_component__WEBPACK_IMPORTED_MODULE_10__["MagicoComponent"],
                    _components_lugares_lugares_component__WEBPACK_IMPORTED_MODULE_11__["LugaresComponent"],
                    _components_buscador_buscador_component__WEBPACK_IMPORTED_MODULE_12__["BuscadorComponent"],
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                    _agm_core__WEBPACK_IMPORTED_MODULE_14__["AgmCoreModule"].forRoot({
                        apiKey: 'AIzaSyDZ1RW72H44t3-lNJyvdqjzIOapDOexa2w'
                    }),
                    _app_routes__WEBPACK_IMPORTED_MODULE_2__["APP_ROUTING"],
                    _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_13__["BrowserAnimationsModule"]
                ],
                providers: [
                    _services_pueblos_service__WEBPACK_IMPORTED_MODULE_3__["PueblosService"]
                ],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "hrlM":
/*!*******************************************************!*\
  !*** ./src/app/components/navbar/navbar.component.ts ***!
  \*******************************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "3Pt+");




const _c0 = function () { return ["home"]; };
const _c1 = function () { return ["lugares"]; };
const _c2 = function () { return ["about"]; };
class NavbarComponent {
    constructor(router) {
        this.router = router;
    }
    // tslint:disable-next-line: typedef
    ngOnInit() {
    }
    // tslint:disable-next-line: typedef
    buscarMagico(termino) {
        //  console.log(termino);
        this.router.navigate(['/buscar', termino]);
    }
}
NavbarComponent.ɵfac = function NavbarComponent_Factory(t) { return new (t || NavbarComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"])); };
NavbarComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NavbarComponent, selectors: [["app-navbar"]], decls: 39, vars: 6, consts: [["rel", "stylesheet", "href", "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css", "integrity", "sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z", "crossorigin", "anonymous"], ["name", "viewport", "content", "width=device-width,user scalable=no, initial-scale=1.0, minimum-scale=1.0"], [1, "navbar", "navbar-expand-lg", "navbar-dark", "bg-dark"], [1, "container-fluid"], ["href", "#", 1, "navbar-brand"], ["src", "assets/img/img1.png", "alt", "", "width", "40", "height", "34"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarSupportedContent", "aria-controls", "navbarSupportedContent", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler"], [1, "navbar-toggler-icon"], ["id", "navbarSupportedContent", 1, "collapse", "navbar-collapse"], [1, "navbar-nav", "mr-auto"], ["routerLinkActive", "active", 1, "nav-item"], [1, "nav-link", 3, "routerLink"], ["routerLinkActive", "active", 1, "nav-item", "dropdown"], ["href", "#", "id", "navbarDropdown", "role", "button", "data-bs-toggle", "dropdown", "aria-expanded", "false", 1, "nav-link", "dropdown-toggle"], ["aria-labelledby", "navbarDropdown", 1, "dropdown-menu"], ["href", "#", 1, "dropdown-item"], [1, "dropdown-divider"], [1, "d-flex"], ["type", "text", "placeholder", "Buscar ", 1, "form-control", "me-2", 3, "keyup.enter"], ["buscarTexto", ""], ["type", "button", 1, "btn", "btn-outline-primary", 3, "click"]], template: function NavbarComponent_Template(rf, ctx) { if (rf & 1) {
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "link", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "meta", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "nav", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "img", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "span", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "ul", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "li", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Inicio");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "li", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "lugares");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "li", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Registrate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "li", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, " Pueblos Magicos ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "ul", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Action");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "Another action");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "hr", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "li");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Something else here");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "form", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "input", 18, 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup.enter", function NavbarComponent_Template_input_keyup_enter_35_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r1); const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](36); return ctx.buscarMagico(_r0.value); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function NavbarComponent_Template_button_click_37_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r1); const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](36); return ctx.buscarMagico(_r0.value); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Buscar");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](3, _c0));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c1));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](5, _c2));
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkActive"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLinkWithHref"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"]], encapsulation: 2 });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NavbarComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-navbar',
                templateUrl: './navbar.component.html',
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }]; }, null); })();


/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "AytR");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map